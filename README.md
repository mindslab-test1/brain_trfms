Brain Transformers
---
여러 엔진에서 공통으로 활용되는 Transformers 구현체들 관리를 위한 repo.

`bert_google_tf1/`
- BERT 공식 구현체(Google)
- forked from github.com/google-research/bert, excluding unnecessary files.

`utils.py`
- utility functions commonly used in NLP engines.
- 나중에 따로 관리될 수도 있음.
